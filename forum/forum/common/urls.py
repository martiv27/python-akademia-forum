from django.urls import path

from forum.common.views import (
    create_category_view,
    delete_category_view,
    index_view,
    register_view,
    update_category_view,
)


app_name = 'common'

urlpatterns = [
    path('', index_view, name='index'),
    path('category/create', create_category_view, name='create_category'),
    path('category/delete/<int:pk>', delete_category_view, name='delete_category'),
    path('category/update/<int:pk>', update_category_view, name='update_category'),

    path('register', register_view, name='register'),
]
