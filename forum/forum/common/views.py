from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import (
    CreateView,
    DeleteView,
    ListView,
    UpdateView,
)

from forum.common.forms import (
    CategoryCreateForm,
    RegisterForm,
)
from forum.common.models import Category


class IndexView(ListView):
    model = Category


index_view = IndexView.as_view()


class CreateCategoryView(SuccessMessageMixin, CreateView):
    model = Category
    form_class = CategoryCreateForm
    success_url = reverse_lazy('common:index')
    success_message = _("Category successfully created")


create_category_view = CreateCategoryView.as_view()


class DeleteCategoryView(SuccessMessageMixin, DeleteView):
    model = Category
    success_url = reverse_lazy('common:index')
    success_message = _("Category successfully deleted")


delete_category_view = DeleteCategoryView.as_view()


class UpdateCategoryView(SuccessMessageMixin, UpdateView):
    model = Category
    form_class = CategoryCreateForm
    success_url = reverse_lazy('common:index')
    template_name_suffix = '_update_form'
    success_message = _("Category successfully updated")


update_category_view = UpdateCategoryView.as_view()


class RegisterView(SuccessMessageMixin, CreateView):
    """ Create new user """
    form_class = RegisterForm
    success_url = reverse_lazy('common:index')
    template_name = "registration/register.html"
    success_message = _("User successfully created")


register_view = RegisterView.as_view()
